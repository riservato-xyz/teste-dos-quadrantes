class TesteQuadrante {

  score = {
    listening: 0,
    speaking: 0,
    writing: 0,
    reading: 0
  }

  questions = [
    {
      text: "Ao ler um livro você:",
      options: [
        {
          text: "Lê atentamente, prestando atenção em cada palavra e fazendo imagens mentais detalhadas dos cenários e personagens descritos.",
          skills: [ "reading" ]
        },
        {
          text: "Se prende a história e a sequência de fatos, mas não consegue muito bem visualizar ou se perde nas descrições.",
          skills: [ "listening" ]
        },
        {
          text: "Lê por cima, pula algumas frases, e pega só o contexto geral, pois quer chegar logo no final.",
          skills: [ "speaking" ]
        },
        {
          text: "Faz pausas quando percebe que a sua mente está vagueando e fica atento as imagens, divisões de parágrafo, pontuações, etc.",
          skills: [ "writing" ]
        }
      ]
    },
    {
      text: "Ao estudar você:",
      options: [
        {
          text: "Fala quando lê (mesmo que baixinho) para memorizar melhor.",
          skills: [ "listening" ]
        },
        {
          text: "É agitado e não consegue ficar muito tempo concentrado em uma mesma atividade.",
          skills: [ "speaking" ]
        },
        {
          text: "Lê bastante e faz mapas mentais, pois essa é a melhor forma de entender a matéria.",
          skills: [ "reading" ]
        },
        {
          text: "Copia toda a lousa, faz resumos da matéria e anotações antes da prova.",
          skills: [ "writing" ]
        }
      ]
    },
    {
      text: "Nas aulas você costuma:",
      options: [
        {
          text: "Fazer muitas anotações escritas.",
          skills: [ 
            "reading",
            "writing"
          ]
        },
        {
          text: "Fazer poucas anotações escritas e lembrar mais da explicação falada.",
          skills: [ 
            "speaking",
            "listening"
          ]
        }
      ]
    },
    {
      text: "Você associa palavras novas que aprendeu em inglês com músicas que já ouviu?",
      options: [
        {
          text: "Sim",
          skills: [ "listening" ]
        },
        {
          text: "Não",
          skills: [ 
            "speaking",
            "reading",
            "listening"
          ]
        }
      ]
    },
    {
      text: "No geral as pessoas te dizem que você é:",
      options: [
        {
          text: "Falante e agitado",
          skills: [ "speaking" ]
        },
        {
          text: "Calmo e contido",
          skills: [ 
            "writing",
            "reading",
            "listening"
          ]
        }
      ]
    },
    {
      text: "Ao assistir um filme você",
      options: [
        {
          text: "Senta e olha para a TV, gosto de filmes com bastante efeitos visuais pois quero prestar atenção as imagens, cores, e descobrir padrões escondidos no filme.",
          skills: [ "reading" ]
        },
        {
          text: "Sabe tudo o que está acontecendo no filme, mesmo se estiver olhando para o celular, fazendo as unhas ou apenas ‘ouvindo’ o filme enquanto faz outras coisas.",
          skills: [ "listening" ]
        },
        {
          text: "Comenta ou pergunta cada cena (ou qualquer outro assunto) com quem está assistindo com você.",
          skills: [ "speaking" ]
        },
        {
          text: "Faço comentários esporádicos e gosto de filmes que apresentem uma sequência lógica ou que expliquem visualmente o que está acontecendo.",
          skills: [ "writing" ]
        }
      ]
    },
    {
      text: "Whatsapp:",
      options: [
        {
          text: "Mensagem de texto",
          skills: [ 
            "reading",
            "writing"
          ]
        },
        {
          text: "Audio",
          skills: [ 
            "listening",
            "speaking"
          ]
        }
      ]
    },
    {
      text: "O seu caderno de estudos",
      options: [
        {
          text: "É super organizado, com canetas coloridas, post-its, divisões e marcações importantes.",
          skills: [ "writing" ]
        },
        {
          text: "É um caderno, eu anoto o que é relevante, as vezes passo a limpo para entender melhor ou revisar o conteúdo.",
          skills: [ 
            "listening",
            "reading"
          ]
        },
        {
          text: "Que caderno?",
          skills: [ "speaking" ]
        }
      ]
    },
    {
      text: "No banho:",
      options: [
        {
          text: "Deixo o Spotify ligado, tem sempre uma música, podcast ou até um vídeo do Youtube rolando enquanto eu tomo banho.",
          skills: [ "listening" ]
        },
        {
          text: "Eu falo sozinho, ou passo o banho todo pensando comigo mesmo em coisas que eu gostaria de falar/fazer, minha mente não para.",
          skills: [ "speaking" ]
        },
        {
          text: "Leio rotulo do shampoo.",
          skills: [ "reading" ]
        },
        {
          text: "Tomo banho ué.",
          skills: [ "writing" ]
        }
      ]
    },
    {
      text: "Você:",
      options: [
        {
          text: "Está sempre ocupado e faz um monte de coisas ao mesmo tempo.",
          skills: [ 
            "listening",
            "speaking"
          ]
        },
        {
          text: "Foca no que é mais importante e prioriza fazer uma coisa por vez.",
          skills: [ 
            "reading",
            "writing"
          ]
        }
      ]
    },
    {
      text: "Você é uma pessoa tímida ou que fala pouco?",
      options: [
        {
          text: "Sim",
          skills: [ "reading" ]
        },
        {
          text: "Não",
          skills: [ 
            "listening",
            "speaking"
          ]
        },
        {
          text: "Depende de onde e com quem estou.",
          skills: [ "writing" ]
        }
      ]
    },
    {
      text: "Você gosta de estudar:",
      options: [
        {
          text: "Com aulas e livros didáticos e matérias visuais como gráficos e planilhas.",
          skills: [ 
            "reading",
            "writing"
          ]
        },
        {
          text: "Com música e filmes.",
          skills: [ "listening" ]
        },
        {
          text: "Não gosto muito de estudar, geralmente aprendo as coisas na vivência.",
          skills: [ "speaking" ]
        }
      ]
    },
    {
      text: "Faça uma redação:",
      options: [
        {
          text: "Ok, quantas palavras? tem limite de linhas e parágrafos?",
          skills: [ "writing" ]
        },
        {
          text: "Ok, mas antes eu vou pesquisar modelos na internet para me inspirar e fazer direitinho.",
          skills: [ "reading" ]
        },
        {
          text: "Posso fazer essa atividade falando?",
          skills: [ "speaking" ]
        },
        {
          text: "Não gosto muito ou acho difícil, então se não tiver outro jeito, vou fazer uma redação básica e de poucas palavras?",
          skills: [ "listening" ]
        }
      ]
    },
    {
      text: "Ouvir nativos falando em inglês",
      options: [
        {
          text: "Eu não entendo tudo, mas eu pesco palavras chaves e consigo entender o que está acontecendo, não me prendo a detalhes, se não entendi algo, eu ignoro e sigo em frente.",
          skills: [ 
            "listening",
            "speaking"
          ]
        },
        {
          text: "Travo quando não entendo uma palavra.",
          skills: [ 
            "writing",
            "reading"
          ]
        }
      ]
    },
    {
      text: "Falar em inglês:",
      options: [
        {
          text: "Fujo e evito o máximo que puder. Tenho medo e/ou vergonha.",
          skills: [ 
            "reading",
            "writing"
          ]
        },
        {
          text: "Ok, talvez eu erre, mas tudo bem porque estou aprendendo e é ótimo poder praticar.",
          skills: [ 
            "listening",
            "speaking"
          ]
        }
      ]
    },
    {
      text: "Você aprende mais",
      options: [
        {
          text: "De forma passiva! Quando o professor explica ou quando leio! Aprendo absorvendo o conteúdo que outras pessoas me passam.",
          skills: [ 
            "listening",
            "reading"
          ]
        },
        {
          text: "De forma ativa! Aprendo melhor quando eu mesmo estou produzindo, seja falando em voz alta, fazendo anotações, pesquisas ou trabalho em grupo.",
          skills: [ 
            "speaking",
            "writing"
          ]
        }
      ]
    },
    {
      text: "Em inglês eu acho difícil:",
      options: [
        {
          text: "Perceber detalhes principalmente na hora de escrever.",
          skills: [ "listening" ]
        },
        {
          text: "Falar, e passo muita vergonha quando eu tento.",
          skills: [ "reading" ]
        },
        {
          text: "Fazer exercícios com muitas informações escritas.",
          skills: [ "speaking" ]
        },
        {
          text: "Entender o que falam, principalmente quando falam rápido.",
          skills: [ "speaking" ]
        }
      ]
    },
    {
      text: "Eu me considero mais:",
      options: [
        {
          text: "Auditivo",
          skills: [ 
            "listening",
            "speaking"
          ]
        },
        {
          text: "Visual",
          skills: [ 
            "reading",
            "writing"
          ]
        }
      ]
    },
    {
      text: "Eu me considero mais",
      options: [
        {
          text: "Generalista",
          skills: [ 
            "listening",
            "speaking"
          ]
        },
        {
          text: "Detalhista",
          skills: [ 
            "reading",
            "writing"
          ]
        }
      ]
    }
  ];

  mainContainer = document.body.querySelector('#testeQuadrante');
  questionContainer = this.mainContainer.querySelector('.questions_container');
  resultContainer = this.mainContainer.querySelector('.results_container');

  controlPrev = this.mainContainer.querySelector('[data-control="previous"]');
  controlNext = this.mainContainer.querySelector('[data-control="next"]');

  constructor(/* data, */ shuffleQuestions = true, shuffleOptions = true) {

    if(shuffleQuestions) this._shuffleQuestions();
    if(shuffleOptions) this._shuffleOptions();

    this.questions.forEach(this.renderQuestion);

    this.controlPrev.addEventListener('click', this._prevControlHandler);
    this.controlNext.addEventListener('click', this._nextControlHandler);
  }

  _prevControlHandler = () => {

    this.controlNext.classList.remove('d-none');

    const question = document.querySelector("[data-current='true']");
    const questionIndex = +question.dataset.index;

    const prevQuestionIndex = questionIndex - 1;
    const prevQuestion = document.querySelector(`[data-index='${prevQuestionIndex}']`);

    switch (questionIndex) {

      case 0:
        this.controlPrev.classList.add('d-none');
        break;

      case 1:
        this.toggleQuestion(question, prevQuestion);

        this.controlPrev.classList.add('d-none');
        this.resultContainer.classList.add('d-none');
        break;
    
      default:

        if(!this.resultContainer.offsetParent) this.toggleQuestion(question, prevQuestion);

        this.controlPrev.classList.remove('d-none');
        this.resultContainer.classList.add('d-none');
        this.questionContainer.classList.remove('d-none');
        break;
    }

    this._subtractScores();
  }

  _nextControlHandler = () => {

    this.controlPrev.classList.remove('d-none');

    const question = document.querySelector("[data-current='true']");
    const questionIndex = +question.dataset.index;

    const nextQuestionIndex = questionIndex + 1;
    const nextQuestion = document.querySelector(`[data-index='${nextQuestionIndex}']`);

    const is_lastQuestion = nextQuestionIndex === this.questions.length;

    this._computeScores(question);

    if ( !is_lastQuestion ) { 

      this.toggleQuestion(question, nextQuestion);
    }
    else {

      const hiScores = Utils.highestInObject(this.score);

      let hiBlocks = Array.from(this.mainContainer.querySelectorAll(`[data-label]`));

      // reset
      hiBlocks.forEach(hiBlock => { hiBlock.classList.remove('highlight') })
      hiBlocks = hiScores.map(hiScore => this.mainContainer.querySelector(`[data-label='${hiScore}']`));
      hiBlocks.forEach(hiBlock => { hiBlock.classList.add('highlight') });

      this.resultContainer.classList.remove('d-none');
      this.questionContainer.classList.add('d-none');
      this.controlNext.classList.add('d-none');
    }

  }

  _shuffleQuestions(question) {

    this.questions = Utils.shuffleArray(this.questions);
  }

  _shuffleOptions() {

    this.questions = this.questions.map(question => {

      question.options = Utils.shuffleArray(question.options);

      return question
    });
  }

  _computeScores() {

    const question = document.querySelector("[data-current='true']");
    let checkedOption = question.querySelector('input[checked]');

    checkedOption = checkedOption.dataset.skills.split(',');
    checkedOption = checkedOption.forEach(skill => { this.score[skill]++ });
  }

  _subtractScores() {

    const question = document.querySelector("[data-current='true']");
    let checkedOption = question.querySelector('input[checked]');

    checkedOption = checkedOption.dataset.skills.split(',');
    checkedOption = checkedOption.forEach(skill => { this.score[skill]-- });
  }

  toggleQuestion(currentQuestion, targetQuestion) {

    currentQuestion.dataset.current = false;
    currentQuestion.classList.add('d-none');

    targetQuestion.dataset.current = true;
    targetQuestion.classList.remove('d-none');
  }

  renderQuestion = (question = this.questions[0], questionIndex) => {

    const container = document.createElement("div");
    const title = document.createElement("h1");
    const options = question.options.map(this.renderOption, questionIndex);

    const is_first = questionIndex == 0;

    title.classList.add('question_title');
    title.innerHTML = `${questionIndex+1} - ${question.text}`;

    container.dataset.index = questionIndex;
    container.classList.add("question");
    container.appendChild(title);

    options.forEach(option => { container.appendChild(option) });

    if(is_first) container.dataset.current = true;
    else container.classList.add('d-none');

    this.questionContainer.appendChild(container);
  }

  renderOption(option, index) {

    index++;
    
    const parentIndex = this;
    const is_first_option = index == 1;

    const container = document.createElement("div");
    const skills = option.skills.join();

    const check = is_first_option ? "checked" : "";

    const markup = `
    <input id="q${parentIndex}_o${index}"
            type="radio" 
            class="btn-check" 
            data-skills="${skills}"
            name="options_q${parentIndex}"  
            autocomplete="off" 
            ${check}>

    <label class="option btn btn-secondary d-block" for="q${parentIndex}_o${index}">
      ${option.text}
    </label>`;

    container.innerHTML = markup;

    return container
  }
}

class Utils {

  // Fisher-Yates
  static shuffleArray(array) {

    let value, index;

    let iteration = array.length;
  
    while (iteration) {
  
      index = Math.floor(Math.random() * iteration--);
  
      value = array[iteration];
      array[iteration] = array[index];
      array[index] = value;
    }
  
    return array;
  }

  /* 
   * it's best to return an array
   * this is important for the case of
   * two entries have the same value.
  */
  static highestInObject(obj) {

    const keys = Object.keys(obj);
    const values = Object.values(obj);

    const max = Math.max.apply(null, values);
    const names = keys.filter(key => obj[key] === max);

    return names
  };
}
